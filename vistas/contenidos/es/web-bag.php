<section class="container-cart bg-white">
    <div class="container container-web-page">
        <h3 class="font-weight-bold poppins-regular text-uppercase">Carrito de compras</h3>
        <hr>
    </div>
    
    <div class="container" style="padding-top: 40px;">

        <div class="row">
            <div class="col-12 col-md-7 col-lg-8">
                <div class="container-fluid">
                <?php
                    require_once "./controladores/bagControlador.php";
                    $ins_bag = new bagControlador();
                    echo $ins_bag->datos_bag_controlador();
                ?>
                    <p class="text-justify">
                        <small>
                            En caso de que no se posea el producto en almacén, los datos del mismo fueran incorrectos o existieran cambios o restricciones por parte de la tienda (precio, inventario, u otras condiciones para la venta) <strong><?php echo COMPANY; ?></strong> se reserva el derecho de cancelar el pedido.
                        </small>
                    </p>
                </div> 
            </div>
            <div class="col-12 col-md-5 col-lg-4">
                <div class="full-box div-bordered">
                    <h5 class="text-center text-uppercase bg-success" style="color: #FFF; padding: 10px 0;">Resumen de la orden</h5>
                    <ul class="list-group bag-details">
                        <a class="list-group-item d-flex justify-content-between align-items-center text-uppercase poppins-regular font-weight-bold">
                            Subtotal
                            <span>
                                <?php 
                                    if(!isset($_SESSION['totalPagar'])){echo "0";}else{
                                    echo COIN_SYMBOL.number_format( $_SESSION['totalPagar'],COIN_DECIMALS,COIN_SEPARATOR_DECIMAL,COIN_SEPARATOR_THOUSAND).' '.COIN_NAME;
                                    }
                                ?>
                            </span>
                        </a>
                        <a class="list-group-item d-flex justify-content-between align-items-center text-uppercase poppins-regular font-weight-bold">
                            Envio
                            <span>
                            <?php 
                                    echo COIN_SYMBOL.number_format( 500,COIN_DECIMALS,COIN_SEPARATOR_DECIMAL,COIN_SEPARATOR_THOUSAND).' '.COIN_NAME;
                            ?>
                            </span>
                        </a>
                        <a class="list-group-item d-flex justify-content-between align-items-center" style="border-bottom: 1px solid #E1E1E1;"></a>
                        <a class="list-group-item d-flex justify-content-between align-items-center text-uppercase poppins-regular font-weight-bold">
                            Total
                            <span>
                                <?php 
                                    if(!isset($_SESSION['totalPagar'])){echo "0";}else{
                                    echo COIN_SYMBOL.number_format( $_SESSION['totalPagar']+500,COIN_DECIMALS,COIN_SEPARATOR_DECIMAL,COIN_SEPARATOR_THOUSAND).' '.COIN_NAME;
                                    }
                                ?>
                            </span>
                        </a>
                    </ul>
                    <?php 
                        if (isset($_SESSION['cliente_nombre'])){
                            echo ('
                            <p class="text-center">
                                <button type="button" class="btn btn-primary">Confirmar pedido</button>
                            </p>');
                        }else{
                            echo ('
                            <p class="text-center">
                            <a href="'.SERVERURL.'signin/" class="btn btn-primary" >Inicar Sesion</a>
                            </p>');
                        }
                       
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>