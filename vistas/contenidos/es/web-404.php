<div class="full-box container-404">
	<div>
		<p class="text-center"><i class="fas fa-rocket fa-10x"></i></p>
		<h1 class="text-center">ERROR 404</h1>
		<p class="lead text-center">Página no encontrada</p>
		<p class="lead text-center"><a href="<?php echo SERVERURL;?>"><i class="fas fa-home"></i> Regresa a home</a></p>
		
	</div>
</div>