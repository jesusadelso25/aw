<?php 
    $peticion_ajax=true;
	require_once "../config/APP.php";
	include "../vistas/inc/session_start.php";

	if(isset($_POST['bag'])){

		/*--------- Instancia al controlador - Instance to controller ---------*/
		require_once "../controladores/bagControlador.php";
        $ins_bag = new bagControlador();
        
        /*--------- Registrar producto - Register product ---------*/
        if($_POST['bag']=="Agregar"){
            echo $ins_bag->agregar_bag_controlador($idProduct);
		}
        if($_POST['bag']=="eliminar"){
            echo $ins_bag->eliminar_bag_controlador($idProduct);
		}
        if($_POST['bag']=="compra"){
            echo $ins_bag->compra_bag_controlador($idclient);
		}
	}else{
		session_destroy();
		header("Location: ".SERVERURL."index/");
	}