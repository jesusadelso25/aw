<?php
	$peticion_ajax=true;
	require_once "../config/APP.php";
	include "../vistas/inc/session_start.php";

	if(isset($_POST['modulo_login'])){

		/*--------- Instancia al controlador - Instance to controller ---------*/
		require_once "../controladores/loginClientControlador.php";
        $ins_login = new loginClientControlador();
        

        /*--------- Cerrar sesion Cliente - Log out administrator ---------*/
        if($_POST['modulo_login']=="loginclient"){
            echo $ins_login->cerrar_sesion_Cliente_controlador();
		}
        

	}else{
		session_destroy();
		header("Location: ".SERVERURL."index/");
	}
?>