<?php 

if($peticion_ajax){
    require_once "../modelos/mainModel.php";
}else{
    require_once "./modelos/mainModel.php";
}

class BagControlador extends mainModel{

    /*--------- Controlador paginador productos (administrador) - Product Pager Controller (admin) ---------*/
    public function datos_bag_controlador(){
        $tabla1="";
        if(!empty($_SESSION['bag'])){
            $total=0;
            foreach($_SESSION['bag'] as $indece=>$bag_List){
                $cout=$bag_List['producto_stock'];
                $tabla1.=' 
                <h5 class="poppins-regular font-weight-bold full-box text-center">'.$bag_List['producto_nombre'].'</h5>
                <div class="bag-item full-box">
                    <figure class="full-box">
                        <img src="'.SERVERURL.'vistas/assets/product/cover/'.$bag_List['producto_portada'].'" class="img-fluid" alt="producto_nombre">
                    </figure>
                    <div class="full-box">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-12 col-lg-6 text-center mb-4">
                                    <div class="row justify-content-center">
                                        <div class="col-auto">
                                            <div class="form-outline mb-4">
                                                <input type="text" value="'.$cout.'" class="form-control text-center" id="product_cant" pattern="[0-9]{1,10}" maxlength="10" style="max-width: 100px; ">
                                                <label for="product_cant" class="form-label">Cantidad</label>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <button type="button" class="btn btn-success" data-mdb-toggle="tooltip" data-mdb-placement="bottom" title="Actualizar cantidad" ><i class="fas fa-sync-alt fa-fw"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-4 text-center mb-4">
                                    <span class="poppins-regular font-weight-bold" >PRECIO UNIDAD:$'.$bag_List['producto_precio_venta'].'</span>
                                    <span class="poppins-regular font-weight-bold" >SUBTOTAL:'.COIN_SYMBOL.number_format($cout*$bag_List['producto_precio_venta'],COIN_DECIMALS,COIN_SEPARATOR_DECIMAL,COIN_SEPARATOR_THOUSAND).' '.COIN_NAME.'</span>
                                </div>
                                <div class="col-12 col-lg-2 text-center text-lg-end mb-4">
                                    <button type="button" class="btn btn-danger" data-mdb-toggle="tooltip" data-mdb-placement="bottom" title="Quitar del carrito" >
                                        <span aria-hidden="true"><i class="far fa-trash-alt"></i></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>';
                $total=$total+$bag_List['producto_stock']*$bag_List['producto_precio_venta'];
                }
        }
        else{
            return $tabla1='
            <div class="container">
            <p class="text-center" ><i class="fas fa-shopping-bag fa-5x"></i></p>
                <h4 class="text-center poppins-regular font-weight-bold" >Carrito de compras vacío</h4>
            </div>';
        }
    $_SESSION['totalPagar']=$total;;
    return $tabla1;
    } 
    /*-- Fin controlador - End controller --*/

    /*--------- Controlador paginador productos (administrador) - Product Pager Controller (admin) ---------*/
    public function agregar_bag_controlador(){
        $IDP=mainModel::decryption($_POST['IDP']);
        $IDP_p=mainModel::decryption($_POST['IDP_p']);
        $IDP_n=mainModel::decryption($_POST['IDP_n']);
        $IDP_c=mainModel::decryption($_POST['IDP_c']);
            if(!isset($_POST['IDP'])&& $IDP==""){
                /*Colcar mesaje si no ocurre el agregar */
            }
            if(!isset( $_SESSION['bag'])){
                $bag_List= array(
                'producto_id'=>$IDP,
                'producto_portada'=> $IDP_p,
                'producto_nombre'=>$IDP_n,
                'producto_stock'=>'1',
                'producto_precio_venta'=> $IDP_c
                );
                $_SESSION['bag'][0]= $bag_List;
            }else{
                foreach($_SESSION['bag'] as $indece=>$bag_List){
                    $comparar=$bag_List['producto_nombre'];
                }
                if($IDP_n==$comparar){
                }else{
                    $Numerobag=count(($_SESSION['bag']));
                    $bag_List= array(
                    'producto_id'=>$IDP,
                    'producto_portada'=> $IDP_p,
                    'producto_nombre'=>$IDP_n,
                    'producto_stock'=>'1',
                    'producto_precio_venta'=> $IDP_c
                );
                $_SESSION['bag'][$Numerobag]= $bag_List;
            }
            
        }
        if(headers_sent()){
            echo "<script> window.location.href='".SERVERURL."product/all/'; </script>";
        }else{
            return header("Location: ".SERVERURL."/product/all/");
        }
    }
    
    /*-- Fin controlador - End controller --*/
    /*--------- Controlador paginador productos (administrador) - Product Pager Controller (admin) ---------*/
    public function eliminar_bag_controlador($idProduct){
        $_SESSION['bag'][0]= $bag_List;

    }
    /*-- Fin controlador - End controller --*/

        /*--------- Controlador paginador productos (administrador) - Product Pager Controller (admin) ---------*/
        public function compra_bag_controlador(){
        }
        /*-- Fin controlador - End controller --*/

    /*--------- Controlador paginador productos (cliente) - Product Pager Controller (client) ---------*/
    public function cliente_paginador_producto_controlador($pagina,$registros,$url,$orden,$categoria,$busqueda){
    $pagina=mainModel::limpiar_cadena($pagina);
    $registros=mainModel::limpiar_cadena($registros);

    $url=mainModel::limpiar_cadena($url);
    $orden=mainModel::limpiar_cadena($orden);
    $categoria=mainModel::limpiar_cadena($categoria);
    $busqueda=mainModel::limpiar_cadena($busqueda);
    $url=SERVERURL.$url."/".$categoria."/".$orden."/";
    $tabla="";


    /*-- Lista blanca para orden de busqueda - Whitelist for search order --*/
    $orden_lista=["ASC","DESC","MAX","MIN"];

    if(!in_array($orden, $orden_lista)){
        return '
            <div class="alert alert-danger text-center" role="alert" data-mdb-color="danger">
                <p><i class="fas fa-exclamation-triangle fa-5x"></i></p>
                <h4 class="alert-heading">¡Ocurrió un error inesperado!</h4>
                <p class="mb-0">Lo sentimos, no podemos realizar la búsqueda de productos ya que al parecer a ingresado un dato incorrecto.</p>
            </div>
        ';
        exit();
    }

    /*-- Estableciendo orden de busqueda - Establishing search order --*/
    if($orden=="ASC" || $orden=="DESC"){
        $campo_orden="producto_nombre $orden";
    }elseif($orden=="MAX" || $orden=="MIN"){
        if($orden=="MAX"){
            $campo_orden="producto_precio_venta DESC";
        }else{
            $campo_orden="producto_precio_venta ASC";
        }
    }else{
        $campo_orden="producto_nombre ASC";
    }
    
    /*-- Comprobando categoria - Checking category --*/
    if($categoria!="all"){
        $check_categoria=mainModel::ejecutar_consulta_simple("SELECT categoria_id FROM categoria WHERE categoria_id='$categoria' AND categoria_estado='Habilitada'");
        if($check_categoria->rowCount()<=0){
            return '
                <div class="alert alert-danger text-center" role="alert" data-mdb-color="danger">
                    <p><i class="fas fa-exclamation-triangle fa-5x"></i></p>
                    <h4 class="alert-heading">¡Ocurrió un error inesperado!</h4>
                    <p class="mb-0">Lo sentimos, no podemos realizar la búsqueda de productos ya que al parecer a ingresado una categoría incorrecta.</p>
                </div>
            ';
            exit();
        }
        $check_categoria->closeCursor();
        $check_categoria=mainModel::desconectar($check_categoria);
    }
    

    $pagina = (isset($pagina) && $pagina>0) ? (int) $pagina : 1;
    $inicio = ($pagina>0) ? (($pagina * $registros)-$registros) : 0;


    $campos="*";
    
    if(isset($busqueda) && $busqueda!=""){
        if($categoria!="all"){
            $condicion_busqueda="categoria_id='$categoria' AND";
        }else{
            $condicion_busqueda="";
        }
        $consulta="SELECT SQL_CALC_FOUND_ROWS $campos FROM producto WHERE $condicion_busqueda producto_estado='Habilitado' AND producto_stock>0 AND producto_nombre LIKE '%$busqueda%' ORDER BY $campo_orden LIMIT $inicio,$registros";
    }elseif($categoria!="all"){
        $consulta="SELECT SQL_CALC_FOUND_ROWS $campos FROM producto WHERE producto_estado='Habilitado' AND categoria_id='$categoria' AND producto_stock>0 ORDER BY $campo_orden LIMIT $inicio,$registros";
    }else{
        $consulta="SELECT SQL_CALC_FOUND_ROWS $campos FROM producto WHERE producto_estado='Habilitado' AND producto_stock>0 ORDER BY $campo_orden LIMIT $inicio,$registros";
    }

    $conexion = mainModel::conectar();

    $datos = $conexion->query($consulta);

    $datos = $datos->fetchAll();

    $total = $conexion->query("SELECT FOUND_ROWS()");
    $total = (int) $total->fetchColumn();

    $Npaginas =ceil($total/$registros);

    $tabla.='<div class="container-cards full-box">';

    if($total>=1 && $pagina<=$Npaginas){
        $contador=$inicio+1;
        $pag_inicio=$inicio+1;
        foreach($datos as $rows){

            $total_price=$rows['producto_precio_venta']-($rows['producto_precio_venta']*($rows['producto_descuento']/100));

            $tabla.='
                <div class="card-product div-bordered bg-white shadow-2">
                    <figure class="card-product-img">';
                        if(is_file("./vistas/assets/product/cover/".$rows['producto_portada'])){
                            $tabla.='<img src="'.SERVERURL.'vistas/assets/product/cover/'.$rows['producto_portada'].'" class="img-fluid" alt="'.$rows['producto_nombre'].'" />';
                        }else{
                            $tabla.='<img src="'.SERVERURL.'vistas/assets/product/cover/default.jpg" class="img-fluid" alt="'.$rows['producto_nombre'].'" />';
                        }
                    $tabla.='</figure>
                    <div class="card-product-body">
                        <div class="card-product-content scroll">
                            <h5 class="text-center fw-bolder">'.mainModel::limitar_cadena($rows['producto_nombre'],70,"...").'</h5>
                            <p class="card-product-price text-center fw-bolder">'.COIN_SYMBOL.number_format($total_price,COIN_DECIMALS,COIN_SEPARATOR_DECIMAL,COIN_SEPARATOR_THOUSAND).' '.COIN_NAME.'</p>';
                            if($rows['producto_tipo']=="Fisico"){
                                $tabla.='<span class="full-box text-center text-muted" style="display: block;">En stock: '.$rows['producto_stock'].'</span>';
                            }
                        $tabla.='
                        </div>
                        <div class="text-center card-product-options" style="padding: 10px 0;">
                            <form action="" method="POST" data-form="save" data-lang="es" autocomplete="off">
                                <button  type="sumbit" class="btn btn-link btn-sm btn-rounded text-success" values="Agregar" name="AgregarL" ><i class="fas fa-shopping-bag fa-fw"></i>&nbsp; Agregar</button>
                                &nbsp; &nbsp;
                                <a href="'.SERVERURL.'details/'.mainModel::encryption($rows['producto_id']).'/" class="btn btn-link btn-sm btn-rounded" ><i class="fas fa-box-open fa-fw"></i> &nbsp; Detalles</a>
                                &nbsp; &nbsp;
                                <button type="button" class="btn btn-link btn-sm btn-rounded"><i class="fas fa-heart fa-fw"></i></button>
                                <input type="hidden" id="IDP" name="IDP" value="'.mainModel::encryption($rows['producto_id']).'">
                                <input type="hidden" id="IDP" name="IDP_p" value="'.mainModel::encryption($rows['producto_portada']).'">
                                <input type="hidden" id="IDP" name="IDP_n" value="'.mainModel::encryption($rows['producto_nombre']).'">
                                <input type="hidden" id="IDP" name="IDP_c" value="'.mainModel::encryption($total_price).'">
                            </form>
                        </div >
                    </div>
                </div>
                ';
                if(isset($_POST['IDP'])){
                    require_once "./controladores/bagControlador.php";
                    $ins_bag= new bagControlador();
                    $ins_bag->agregar_bag_controlador();
                }
                
                
                '
            ';
            
            $contador++;
        }
        $pag_final=$contador-1;
    }else{
        if($total>=1){
            $tabla.='
                <div class="alert alert-default text-center" role="alert" data-mdb-color="danger">
                    <p><i class="fas fa-boxes fa-fw fa-5x"></i></p>
                    <h4 class="alert-heading">Haga clic en el botón para listar nuevamente los productos que están registrados en la tienda.</h4>
                    <a href="'.$url.'" class="btn btn-primary btn-rounded btn-lg" data-mdb-ripple-color="dark">Haga clic acá para recargar el listado</a>
                </div>
            ';
        }else{
            $tabla.='
                <div class="alert alert-default text-center" role="alert" data-mdb-color="danger">
                    <p><i class="fas fa-broadcast-tower fa-fw fa-5x"></i></p>
                    <h4 class="alert-heading">¡No hay productos en inventario!</h4>
                    <p class="mb-0">No hemos encontrado productos registrados en la tienda.</p>
                </div>
            ';
        }
    }

    $tabla.='</div>';

    if($total>0 && $pagina<=$Npaginas){
        $tabla.='<p class="text-end">Mostrando productos <strong>'.$pag_inicio.'</strong> al <strong>'.$pag_final.'</strong> de un <strong>total de '.$total.'</strong></p>';
    }

    /*--Paginacion - Pagination --*/
    if($total>=1 && $pagina<=$Npaginas){
        $tabla.=mainModel::paginador_tablas($pagina,$Npaginas,$url,7,LANG);
    }

    return $tabla;
} /*-- Fin controlador - End controller --*/

}

?>