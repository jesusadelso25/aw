<?php

	if($peticion_ajax){
		require_once "../modelos/mainModel.php";
	}else{
		require_once "./modelos/mainModel.php";
	}

	class loginClientControlador extends mainModel{

		/*----------  Controlador iniciar sesion Cliente - Controller login administrator ----------*/
		public function iniciar_sesion_Cliente_controlador(){

			$Cliente=mainModel::limpiar_cadena($_POST['login_email']);
			$clave=mainModel::limpiar_cadena($_POST['login_clave']);

			/*-- Comprobando campos vacios - Checking empty fields --*/
			if($Cliente=="" || $clave==""){
				echo'<script>
					Swal.fire({
					    title: "Ocurrió un error inesperado",
					    text: "No has llenado todos los campos que son requeridos.",
					    icon: "error",
					    confirmButtonText: "Aceptar"
					});
				</script>';
				exit();
			}

			if(mainModel::verificar_datos("[a-zA-Z0-9$@.-]{7,100}",$clave)){
				echo'<script>
					Swal.fire({
					    title: "Ocurrió un error inesperado",
					    text: "La contraseña no coincide con el formato solicitado.",
					    icon: "error",
					    confirmButtonText: "Aceptar"
					});
				</script>';
				exit();
			}

			$clave=mainModel::encryption($clave);

			/*-- Verificando datos de la cuenta - Verifying account details --*/
			$datos_cuenta=mainModel::datos_tabla("Normal","cliente WHERE cliente_email='$Cliente' AND 	cliente_clave='$clave' AND cliente_cuenta_estado='Activa'","*",0);

			if($datos_cuenta->rowCount()==1){

				$row=$datos_cuenta->fetch();

				$datos_cuenta->closeCursor();
			    $datos_cuenta=mainModel::desconectar($datos_cuenta);
				$_SESSION['cliente_id']=$row['cliente_id'];
				$_SESSION['cliente_foto']=$row['cliente_foto'];
				$_SESSION['cliente_nombre']=$row['cliente_nombre'];
				$_SESSION['cliente_apellido']=$row['cliente_apellido'];
				$_SESSION['cliente_genero']=$row['cliente_genero'];
				$_SESSION['cliente_telefono']=$row['cliente_telefono'];
				$_SESSION['cliente_provincia']=$row['cliente_provincia'];
				$_SESSION['cliente_ciudad']=$row['cliente_ciudad'];
				$_SESSION['cliente_direccion']=$row['cliente_direccion'];
				$_SESSION['cliente_email']=$row['cliente_email'];
				$_SESSION['cliente_clave']=$row['cliente_clave'];
				$_SESSION['cliente_cuenta_estado']=$row['cliente_cuenta_estado'];
				$_SESSION['token_sto']=mainModel::encryption(uniqid(mt_rand(), true));
				if(headers_sent()){
					echo "<script> window.location.href='".SERVERURL."index/'; </script>";
				}else{
					return header("Location: ".SERVERURL."/index/");
				} 
			}else{
				echo'<script>
					Swal.fire({
					    title: "Datos incorrectos",
					    text: "El nombre de usuario o contraseña no son correctos.",
					    icon: "error",
					    confirmButtonText: "Aceptar"
					});
				</script>';
			}
		} /*-- Fin controlador - End controller --*/


		/*----------  Controlador forzar cierre de sesion - Controller force logout ----------*/
		public function forzar_cierre_sesion_controlador(){
			session_unset();
			session_destroy();
			if(headers_sent()){
				echo "<script> window.location.href='".SERVERURL."index/'; </script>";
			}else{
				return header("Location: ".SERVERURL."index/");
			}
		} /*-- Fin controlador - End controller --*/


		/*----------  Controlador cierre de sesion administrador - Controller logout administrator  ----------*/
		public function cerrar_sesion_Cliente_controlador(){
			if(isset($_SESSION['cliente_nombre'])){
				unset($_SESSION['cliente_id']);
				unset($_SESSION['cliente_foto']);
				unset($_SESSION['cliente_nombre']);
				unset($_SESSION['cliente_apellido']);
				unset($_SESSION['cliente_genero']);
				unset($_SESSION['cliente_telefono']);
				unset($_SESSION['cliente_provincia']);
				unset($_SESSION['cliente_ciudad']);
				unset($_SESSION['cliente_direccion']);
				unset($_SESSION['cliente_email']);
				unset($_SESSION['cliente_clave']);
				session_destroy();	
				
				$alerta=[
					"Alerta"=>"redireccionar",
					"URL"=>SERVERURL."index/"
				];
			}else{
				$alerta=[
					"Alerta"=>"simple",
					"Titulo"=>"Ocurrió un error inesperado",
					"Texto"=>"No se pudo cerrar la sesión",
					"Icon"=>"error",
					"TxtBtn"=>"Aceptar"
				];
			}
			echo json_encode($alerta);
		} /*-- Fin controlador - End controller --*/
	}