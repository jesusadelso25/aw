<?php
	/*----------  
	Ruta o dominio del servidor  - Server path or domain
	----------*/
	const SERVERURL="http://localhost/AW/";


	/*----------  
	Nombre de la empresa o compañia - Descricion de su servicios
	----------*/
	const COMPANY="A&W Celurales";
    const DESCRIENVIOS="Los agitados ritmos de vida que como sociedad estamos llevando tienen como consecuencia que los consumidores cada vez busquen más servicios que se ajusten a sus necesidades y que, en pocas palabras, les faciliten la vida. Con esta estrategia de envíos a domicilio, estaremos ofreciendo justo lo que los clientes están buscando.";
	const DESCRIVENTAS="Cómo empresario mayorista podrás comprar y vender a precios muy bajos en relación al mercado, esto te permitirá conseguir clientes más rápido, sin embargo, es importante cultivar los socios ganados para mantener una distribución constante.";
	const DESCRIRETIRO="Comprar por Internet y buscar la mercadería en la tienda, ofrece a los consumidores habituados al shopping en línea lo mejor de dos mundos: no solo tienen el placer de elegir y de visitar la tienda, sino que ahorran tiempo mientras llenan el carrito con los productos que adquieren habitualmente.";


	/*----------  Idioma - Language
	Español -> es 
	----------*/
	const LANG="es";

	
	/*----------  
		Palabra clave dashboard - Dashboard keyword
	----------*/
	const DASHBOARD="AW";


	/*----------  
	Nombre de la sesion -  Session name
	----------*/
	const SESSION_NAME="STO";


	/*----------  Redes sociales - Social networks  ----------*/
	const FACEBOOK="https://www.facebook.com/";
	const INSTAGRAM="https://www.facebook.com/";
	const YOUTUBE="https://www.youtube.com/";
	const TWITTER="https://www.facebook.com/";


	/*----------  Direccion - Address  ----------*/
	const COUNTRY="Republica Dominicana";
	const ADDRESS="Autopista Duarte, La vega,Santiago";
	

	/*----------  Configuración de moneda - Currency Settings  ----------*/
	const COIN_SYMBOL="$";
	const COIN_NAME="Peso";
	const COIN_DECIMALS="2";
	const COIN_SEPARATOR_THOUSAND=",";
	const COIN_SEPARATOR_DECIMAL=".";


	/*----------  Tipos de documentos - Document types ----------*/
	const DOCUMENTS_USERS=["DNI","Cedula","DUI","Licencia","Pasaporte","Otro"];
	const DOCUMENTS_COMPANY=["DNI","Cedula","RUT","NIT","RUC","Otro"];


	/*----------  Tipos de unidades de productos - Types of product units ----------*/
	const PRODUTS_UNITS=["Gorra","Bolsa","Estuche","Mochila","Otro"];

	/*----------  Límite de tamaño de imágenes de productos en MB - Product image size limit in MB ----------*/
	const COVER_PRODUCT=3;
	const GALLERY_PRODUCT=7;


	/*----------  Marcador de campos obligatorios - Mandatory field marker  ----------*/
	const FIELD_OBLIGATORY='&nbsp; <i class="fab fa-font-awesome-alt"></i> &nbsp;';


	/*----------  Configuración de codigos de barras - Bar code settings

		BARCODE_FORMAT -> CODE128 | CODE39 | EAN | EAN-13 | EAN-8 | EAN-5 | EAN-2 | UPC | ITF | ITF-14 | MSI | MSI10 | MSI11 | MSI1010 | MSI1110 | Pharmacode

		BARCODE_TEXT_ALIGN -> center | left | right

		BARCODE_TEXT_POSITION -> top | bottom

	----------*/

	const BARCODE_FORMAT="CODE128";
	const BARCODE_TEXT_ALIGN="center";
	const BARCODE_TEXT_POSITION="bottom";


	/*----------  Tamaño de papel de impresora termica (en milimetros) - Thermal printer paper size (in millimeters)
		THERMAL_PRINT_SIZE -> 80 | 57
	----------*/
	const THERMAL_PRINT_SIZE="80";


	/*----------  Zona horaria - Time zone  ----------*/
	date_default_timezone_set("America/El_Salvador");

	/*
		Configuración de zona horaria de tu país, para más información visita - Time zone configuration of your country, for more information visit
		
		http://php.net/manual/es/function.date-default-timezone-set.php
		http://php.net/manual/es/timezones.php
	*/